A script for generating a population of synthetic users for testing the Band Together app.

* makeFakeData.py: the main script
    * requires the Python modules [faker](https://pypi.python.org/pypi/fake-factory), [numpy](http://www.numpy.org), and [pandas](http://pandas.pydata.org)
* veterans_location.csv: breakdowns by state (read by makeFakeData.py)
* veterans_demographics.csv: breakdowns by age, gender, branch (not read by makeFakeData.py; data is hard coded)
* img/: folder of dummy profile pics, sorted into m/ and f/
* userList.txt: the output of makeFakeData.py, in JSON format