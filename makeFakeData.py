from faker import Factory
import numpy as np
import pandas
import json

##
## Settings
##

numUsers = 2000
outFileName = "userList.txt"
locationFileName = "veterans_location.csv"

##
## Functions for generating fields in user profiles
##

def chooseFromList(options, weights):
    """Choose one of N options given N weights"""
    probabilities = np.zeros(len(options)-1)
    for index in range(len(probabilities)):
        probabilities[index] = weights[index]/np.sum(weights)

    cutoffs = np.zeros_like(probabilities)
    for index in range(len(cutoffs)):
        cutoffs[index] = np.sum(probabilities[0:index+1])

    value = np.random.uniform()
    choice = options[len(options)-1]
    for index in np.array(range(len(cutoffs), 0, -1)) - 1:
        if (value < cutoffs[index]):
            choice = options[index]
    return choice

def getAge():
    """Return an age sampled from some plausible distribution"""
    minAge = 18
    maxAge = 65
    alpha = 2
    beta = 5
    age = (maxAge-minAge)*np.random.beta(alpha, beta) + minAge
    return int(age)

def getGender():
    """Return a gender"""
    gender = chooseFromList(['male', 'female'], [0.8, 0.2])
    return gender

def getSkills():
    """Return a random number of skills"""
    maxNumSkills = 4
    options = ['woodworking', 'accounting', 'programming', 'writing', 'guitar', 'Six Sigma', 'Spanish', 'fishing', 'cooking', 'welding', 'salsa', 'camping', 'MMA', 'running', 'biking', 'chess', 'dancing', 'ultimate frisbee', 'hockey', 'bird watching', 'German', 'basketball', 'shooting', 'yoga', 'machining', 'improv', 'painting', 'photography', 'piano']

    skills = []
    numSkills = int(np.round(maxNumSkills*np.random.uniform() - 0.5))
    for index in range(numSkills):
        newSkill = options.pop(int(np.round(np.random.uniform()*len(options) - 0.5)))
        skills.append(newSkill)

    return skills

def getBranch():
    """Return a military branch"""
    options = ['Army', 'Navy', 'Marines', 'Air Force', 'Non-DoD', 'Reserve']
    weights = [0.46, 0.20, 0.12, 0.16, 0.01, 0.04]
    branch = chooseFromList(options, weights)
    return branch

def getAveLogins():
    """Return an average number of logins per period"""
    minAveLogins = 0
    maxAveLogins = 20
    alpha = 2
    beta = 3
    aveLogins = (maxAveLogins-minAveLogins)*np.random.beta(alpha, beta) + minAveLogins
    return aveLogins

def getLocation():
    """Return a latitude and longitude"""
    l = pandas.read_csv(locationFileName)
    l.loc[l.State=='Alaska', 'LongH'] = -180 # hacky fix for Alaska wrapping around the IDL

    options = range(len(l))
    weights = [float(x) for x in l.Fraction]
    i = chooseFromList(options, weights)
    location = {
            'lat': (l.LatH[i]-l.LatL[i])*np.random.uniform() + l.LatL[i],
            'lon': (l.LongL[i]-l.LongH[i])*np.random.uniform() + l.LongH[i],
        }

    return location

def getProfilePic(gender):
    """Return a profile picture"""
    if gender == "male":
        profileno = np.random.randint(0,99)
        directory = "m"
    else:
        profileno = np.random.randint(0,97)
        directory = "f"
    profilepic = "%s/%d.jpg" % (directory, profileno)
    return profilepic

def createProfile():
    """Return a dictionary representing a user profile"""

    profile = {}
    faker = Factory.create()

    # gender
    profile['gender'] = getGender()

    # first name
    if profile['gender'] == 'male':
        profile['first'] = faker.first_name_male()
    else:
        profile['first'] = faker.first_name_female()

    # last name
    profile['last'] = faker.last_name()

    # username
    profile['username'] = (profile['first'][0] + profile['last']).lower()

    # profile picture
    profile['profilepic'] = getProfilePic(profile['gender'])

    # age
    profile['age'] = getAge()

    # location
    profile['location'] = getLocation()

    # branch
    profile['branch'] = getBranch()

    # skills
    profile['skills'] = getSkills()

    # want to learn
    profile['wantToLearn'] = getSkills()

    # mentor?
    profile['mentor'] = chooseFromList([True, False], [0.1, 0.9])

    # logins per day this period
    profile['avgLoginCurrentPeriod'] = getAveLogins()

    # logins per day last period
    profile['avgLoginPreviousPeriod'] = getAveLogins()

    # tours

    return profile

##
## Generate users
##

userList = []

for index in range(0, numUsers):
    userList.append(createProfile());

##
## Write to file
##

with open(outFileName, 'w') as outFile:
    json.dump(userList, outFile)
